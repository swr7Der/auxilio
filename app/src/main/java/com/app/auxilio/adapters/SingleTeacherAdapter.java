package com.app.auxilio.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.auxilio.R;
import com.app.auxilio.activities.HomeScreenActivity;
import com.app.auxilio.helper_class.SingleTeacher;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

/**
 * Created by swarnim on 16/6/17.
 */

public class SingleTeacherAdapter extends RecyclerView.Adapter<SingleTeacherAdapter.singleTeacherHolder> {

    Context mContext;
    ArrayList<SingleTeacher> singleTeacherArrayList;

    public SingleTeacherAdapter(Context mContext, ArrayList<SingleTeacher> singleTeacherArrayList) {
        this.mContext = mContext;
        this.singleTeacherArrayList = singleTeacherArrayList;
    }

    @Override
    public singleTeacherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.z_single_teacher,parent,false);
        return new singleTeacherHolder(view);
    }

    @Override
    public void onBindViewHolder(final singleTeacherHolder holder, int position) {

        SingleTeacher singleTeacher = singleTeacherArrayList.get(position);

        String gender = null;

        if (singleTeacher.getGender() == 0){
            gender = "Male";

            Log.i("Result_adapter",singleTeacher.getProfileImage());
            Glide.with(mContext)
                    .load(singleTeacher.getProfileImage())
                    .placeholder(R.drawable.male_profile_pic)
                    .error(R.drawable.male_profile_pic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.proPic);
        } else {

            if (singleTeacher.getGender() == 1){
                gender = "Female";
            } else
                gender = "Not verified";

            Glide.with(mContext)
                    .load(singleTeacher.getProfileImage())
                    .placeholder(R.drawable.female_profile_pic)
                    .error(R.drawable.female_profile_pic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.proPic);

        }

        holder.name.setText(singleTeacher.getName());
        String shortDetail = singleTeacher.getAvailable_as()+" upto Class "+singleTeacher.getClass_upto()+" in "+singleTeacher.getLocation();
        holder.shortDetails.setText(shortDetail);

        String teachingDetail = "Available as: "+singleTeacher.getAvailable_as()+
                "\nEducational Qualification: "+singleTeacher.getEdu_qualification()+
                "\nSpeciality in: "+singleTeacher.getSpeciality_in()+
                "\nExperience: "+singleTeacher.getExperience()+
                " years\nClass upto he/she can teach: "+singleTeacher.getClass_upto()+
                "\nHe/she can teach: "+singleTeacher.getTeaching_sub();

        holder.teachingDetails.setText(teachingDetail);

        String personalDetail = "Gender: "+gender+"\nEmail: "+singleTeacher.getEmail()+
                "\nMob: "+singleTeacher.getMob()+"\nLocation: "+singleTeacher.getLocation()+
                "\nAddress: "+singleTeacher.getAddress();

        holder.personalDetails.setText(personalDetail);

        holder.shortDetailsRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.view.getVisibility() == View.GONE){
                    holder.view.setVisibility(View.VISIBLE);
                    holder.detailsLl.setVisibility(View.VISIBLE);
                } else {
                    holder.detailsLl.setVisibility(View.GONE);
                    holder.view.setVisibility(View.GONE);
                }
            }
        });

        holder.showProfileDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //holder.personalDetails.setVisibility(View.VISIBLE);
                //holder.showProfileDetails.setVisibility(View.GONE);
                ((HomeScreenActivity)mContext).showRewardedAdIfLoaded();
            }
        });

    }

    @Override
    public int getItemCount() {
        return singleTeacherArrayList.size();
    }

    public class singleTeacherHolder extends RecyclerView.ViewHolder {

        RelativeLayout shortDetailsRl;
        LinearLayout detailsLl;
        ImageView proPic;
        TextView name,shortDetails,teachingDetails,personalDetails;
        View view;
        Button showProfileDetails;

        public singleTeacherHolder(View itemView) {
            super(itemView);

            shortDetailsRl = (RelativeLayout) itemView.findViewById(R.id.shortDetailsRl);
            detailsLl = (LinearLayout) itemView.findViewById(R.id.detailsLL);
            proPic = (ImageView) itemView.findViewById(R.id.proPic);
            view = (View) itemView.findViewById(R.id.view);
            name = (TextView) itemView.findViewById(R.id.name);
            shortDetails = (TextView) itemView.findViewById(R.id.shortDetail);
            teachingDetails = (TextView) itemView.findViewById(R.id.teachingDetails);
            personalDetails = (TextView) itemView.findViewById(R.id.personalDetails);
            showProfileDetails = (Button) itemView.findViewById(R.id.showPersonalDetails);

        }
    }
}
