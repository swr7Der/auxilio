package com.app.auxilio.constants;

/**
 * Created by swarnim on 5/6/17.
 */

public class Constants {

    //public static final String base_url = "http://10.0.3.2:8070/";
    public static final String base_url = "https://metric-speeder.000webhostapp.com/public/";

    public static final String storage = "com.app.auxilio";
    public static final String loginDetails = "loginDetails";
    public static final String loginEmail = "loginEmail";
    public static final String loginPw = "loginPw";
    public static final String loginUserType = "loginUserType";

}
