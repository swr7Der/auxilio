package com.app.auxilio.helper_class;

/**
 * Created by swarnim on 16/6/17.
 */

public class SingleTeacher {

    private int id,gender, status, experience, voting_count;
    private String name, email, mob, location, profileImage, idCard,apiKey,userType, available_as, edu_qualification,
            speciality_in, class_upto, teaching_sub, address;
    private double rating;

    public SingleTeacher(int id, int gender, int status, int experience, int voting_count, String name,
                         String email, String mob, String location, String profileImage, String idCard,
                         String apiKey, String userType, String available_as, String edu_qualification,
                         String speciality_in, String class_upto, String teaching_sub, String address,
                         double rating) {
        this.id = id;
        this.gender = gender;
        this.status = status;
        this.experience = experience;
        this.voting_count = voting_count;
        this.name = name;
        this.email = email;
        this.mob = mob;
        this.location = location;
        this.profileImage = profileImage;
        this.idCard = idCard;
        this.apiKey = apiKey;
        this.userType = userType;
        this.available_as = available_as;
        this.edu_qualification = edu_qualification;
        this.speciality_in = speciality_in;
        this.class_upto = class_upto;
        this.teaching_sub = teaching_sub;
        this.address = address;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getVoting_count() {
        return voting_count;
    }

    public void setVoting_count(int voting_count) {
        this.voting_count = voting_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAvailable_as() {
        return available_as;
    }

    public void setAvailable_as(String available_as) {
        this.available_as = available_as;
    }

    public String getEdu_qualification() {
        return edu_qualification;
    }

    public void setEdu_qualification(String edu_qualification) {
        this.edu_qualification = edu_qualification;
    }

    public String getSpeciality_in() {
        return speciality_in;
    }

    public void setSpeciality_in(String speciality_in) {
        this.speciality_in = speciality_in;
    }

    public String getClass_upto() {
        return class_upto;
    }

    public void setClass_upto(String class_upto) {
        this.class_upto = class_upto;
    }

    public String getTeaching_sub() {
        return teaching_sub;
    }

    public void setTeaching_sub(String teaching_sub) {
        this.teaching_sub = teaching_sub;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
