package com.app.auxilio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.auxilio.R;
import com.app.auxilio.constants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    FrameLayout frame1,frame2;
    EditText name,email,pw,retypePW,mobNumber,eduQual,speciality,experience,classUpto,teachingSub,address;
    Spinner sex,userType,availAs,location;
    Button proPicButton,idCardButton,prevButton,nextButton;
    ImageView proPic,idCard;
    TextView signInNow;
    ProgressBar progressBar;

    private int PICK_PROFILE_PIC = 1;
    private int PICK_ID_PIC = 2;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        frame1 = (FrameLayout) findViewById(R.id.frame1);
        frame2 = (FrameLayout) findViewById(R.id.frame2);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        pw = (EditText) findViewById(R.id.pw);
        retypePW = (EditText) findViewById(R.id.retypePW);
        mobNumber = (EditText) findViewById(R.id.mobNumber);
        eduQual = (EditText) findViewById(R.id.eduQual);
        speciality = (EditText) findViewById(R.id.speciality);
        experience = (EditText) findViewById(R.id.experience);
        classUpto = (EditText) findViewById(R.id.classUpto);
        teachingSub = (EditText) findViewById(R.id.teachingSub);
        address = (EditText) findViewById(R.id.address);
        sex = (Spinner) findViewById(R.id.sex);
        userType = (Spinner) findViewById(R.id.userType);
        availAs = (Spinner) findViewById(R.id.availAs);
        location = (Spinner) findViewById(R.id.location);
        prevButton = (Button) findViewById(R.id.prevButton);
        nextButton = (Button) findViewById(R.id.nextButton);
        signInNow = (TextView) findViewById(R.id.signInNow);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        sharedPreferences = getSharedPreferences(Constants.storage,MODE_PRIVATE);

        signInNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(i);
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frame1.getVisibility() == View.VISIBLE){
                    Toast.makeText(RegistrationActivity.this,"No previous page available",Toast.LENGTH_SHORT).show();
                } else if (frame2.getVisibility() == View.VISIBLE){
                    frame2.setVisibility(View.GONE);
                    frame1.setVisibility(View.VISIBLE);
                    progressBar.setProgress(50);
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frame1.getVisibility() == View.VISIBLE){
                    if (checkForConditions1()){
                        if (userType.getSelectedItemPosition()==1){
                            frame1.setVisibility(View.GONE);
                            frame2.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                progressBar.setProgress(100,true);
                            } else {
                                progressBar.setProgress(100);
                            }
                        } else {
                            registerNow();
                        }
                    }
                } else if (frame2.getVisibility() == View.VISIBLE){
                    if (checkForConditions2()){
                        registerNow();
                    }
                }
            }
        });
    }

    public boolean checkForConditions1(){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!String.valueOf(name.getText()).isEmpty() &&
                !String.valueOf(email.getText()).isEmpty() &&
                !String.valueOf(pw.getText()).isEmpty() &&
                !String.valueOf(retypePW.getText()).isEmpty() &&
                sex.getSelectedItemPosition()!=0 &&
                userType.getSelectedItemPosition()!=0 &&
                !String.valueOf(mobNumber.getText()).isEmpty() &&
                location.getSelectedItemPosition()!=0){

            if (!String.valueOf(pw.getText()).equals(String.valueOf(retypePW.getText()))){
                Toast.makeText(this,"Password doesn't match",Toast.LENGTH_SHORT).show();
            } else if (!String.valueOf(email.getText()).matches(emailPattern) ||
                    !String.valueOf(email.getText()).endsWith("gmail.com")){
                Toast.makeText(this,"Please enter valid email address (Only registration through Gmail id is allowed)",Toast.LENGTH_SHORT).show();
            } else if (String.valueOf(mobNumber.getText()).length() != 10){
                Toast.makeText(this,"Please enter valid 10-digit mobile number",Toast.LENGTH_SHORT).show();
            } else {
                return true;
            }
            return false;
        } else {
            Toast.makeText(this,"All fields are necessary",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public boolean checkForConditions2(){
        if (!String.valueOf(eduQual.getText()).isEmpty() &&
                !String.valueOf(speciality.getText()).isEmpty() &&
                !String.valueOf(experience.getText()).isEmpty() &&
                !String.valueOf(classUpto.getText()).isEmpty() &&
                !String.valueOf(teachingSub.getText()).isEmpty() &&
                !String.valueOf(address.getText()).isEmpty() &&
                availAs.getSelectedItemPosition()!=0){
            return true;
        } else {
            Toast.makeText(this,"All fields are necessary",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void registerNow(){
        progressBar.setProgress(100);

        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Registering...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String user = null;
        if (userType.getSelectedItemPosition() == 1){
            user = "teachers";
        } else if (userType.getSelectedItemPosition() == 2){
            user = "normal_user";
        }

        final String finalUser = user;
        if (userType.getSelectedItemPosition() == 1){
            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    Constants.base_url + "v1/register/teachers",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hide();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Toast.makeText(RegistrationActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                if (!jsonObject.getBoolean("error")){
                                    login(String.valueOf(email.getText()),String.valueOf(pw.getText()), finalUser);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("Auxi",response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.hide();
                    error.printStackTrace();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("name",String.valueOf(name.getText()));
                    params.put("email",String.valueOf(email.getText()));
                    params.put("password",String.valueOf(pw.getText()));
                    params.put("gender",String.valueOf(sex.getSelectedItemPosition()-1));
                    params.put("mob",String.valueOf(mobNumber.getText()));
                    params.put("profile_image","null");
                    params.put("id_card","null");
                    params.put("user_type", finalUser);
                    params.put("available_as",String.valueOf(availAs.getSelectedItem()));
                    params.put("edu_qualification",String.valueOf(eduQual.getText()));
                    params.put("speciality_in",String.valueOf(speciality.getText()));
                    params.put("experience",String.valueOf(experience.getText()));
                    params.put("class_upto",String.valueOf(classUpto.getText()));
                    params.put("teaching_sub",String.valueOf(teachingSub.getText()));
                    params.put("address",String.valueOf(address.getText()));
                    params.put("location",String.valueOf(location.getSelectedItem()));
                    return params;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(stringRequest);

        } else {

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    Constants.base_url + "v1/register/normal_user",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.hide();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Toast.makeText(RegistrationActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                if (!jsonObject.getBoolean("error")){
                                    login(String.valueOf(email.getText()),String.valueOf(pw.getText()), finalUser);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("Auxi",response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.hide();
                    error.printStackTrace();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("name",String.valueOf(name.getText()));
                    params.put("email",String.valueOf(email.getText()));
                    params.put("password",String.valueOf(pw.getText()));
                    params.put("gender",String.valueOf(sex.getSelectedItemPosition()-1));
                    params.put("mob",String.valueOf(mobNumber.getText()));
                    params.put("profile_image","null");
                    params.put("id_card","null");
                    params.put("location",String.valueOf(location.getSelectedItem()));
                    return params;
                }
            };

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(stringRequest);

        }

        /*Bitmap bitmap = ((BitmapDrawable) proPic.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] bytes = baos.toByteArray();

        String encodedImage = Base64.encodeToString(bytes,Base64.DEFAULT);

        Log.i("Result",encodedImage);*/
    }

    public void login(final String email, final String pw, final String userType){
        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Logging in...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (!jsonObject.getBoolean("error")){

                                editor = sharedPreferences.edit();
                                editor.putString(Constants.loginDetails,response);
                                editor.putString(Constants.loginEmail,email);
                                editor.putString(Constants.loginPw,pw);
                                editor.putString(Constants.loginUserType,userType);
                                editor.apply();

                                Intent i = new Intent(RegistrationActivity.this,HomeScreenActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(i);
                            } else {
                                Toast.makeText(RegistrationActivity.this,jsonObject.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("Auxi",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email",email);
                params.put("password",pw);
                params.put("table_name", userType);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }
}
