package com.app.auxilio.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.auxilio.R;
import com.app.auxilio.constants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {

    TextView toRegisterPage;
    EditText email,pw;
    Spinner userType;
    Button loginNow;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toRegisterPage = (TextView) findViewById(R.id.toRegisterPage);
        email = (EditText) findViewById(R.id.email);
        pw = (EditText) findViewById(R.id.password);
        userType = (Spinner) findViewById(R.id.userType);
        loginNow = (Button) findViewById(R.id.loginNow);

        sharedPreferences = getSharedPreferences(Constants.storage,MODE_PRIVATE);

        if (sharedPreferences.contains(Constants.loginDetails)){
            if (sharedPreferences.getString(Constants.loginDetails,null)!= null){
                login(sharedPreferences.getString(Constants.loginEmail,null),
                        sharedPreferences.getString(Constants.loginPw,null),
                        sharedPreferences.getString(Constants.loginUserType,null));
            }
            Log.i("Result",sharedPreferences.getString(Constants.loginDetails,null));
        }

        toRegisterPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(i);
            }
        });

        loginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCondition()){

                    String user = null;
                    if (userType.getSelectedItemPosition() == 1){
                        user = "teachers";
                    } else if (userType.getSelectedItemPosition() == 2){
                        user = "normal_user";
                    }

                    login(String.valueOf(email.getText()),String.valueOf(pw.getText()),user);
                }
            }
        });
    }

    public boolean checkCondition(){
        if (!String.valueOf(email.getText()).isEmpty() &&
                !String.valueOf(pw.getText()).isEmpty() &&
                userType.getSelectedItemPosition() != 0){
            return true;
        } else {
            Toast.makeText(this,"All fields are necessary",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void login(final String email, final String pw, final String userType){
        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Logging in...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (!jsonObject.getBoolean("error")){

                                editor = sharedPreferences.edit();
                                editor.putString(Constants.loginDetails,response);
                                editor.putString(Constants.loginEmail,email);
                                editor.putString(Constants.loginPw,pw);
                                editor.putString(Constants.loginUserType,userType);
                                editor.apply();

                                Intent i = new Intent(LoginActivity.this,HomeScreenActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(i);
                            } else {
                                Toast.makeText(LoginActivity.this,jsonObject.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("Auxi",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email",email);
                params.put("password",pw);
                params.put("table_name", userType);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }
}
