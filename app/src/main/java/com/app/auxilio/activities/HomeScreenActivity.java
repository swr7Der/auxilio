package com.app.auxilio.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.auxilio.R;
import com.app.auxilio.adapters.SingleTeacherAdapter;
import com.app.auxilio.constants.Constants;
import com.app.auxilio.helper_class.SingleTeacher;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean exit = false;

    ImageView proPic;
    TextView nameOfUser, emailOfUser;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private int PICK_PROFILE_PIC = 1;
    private int PICK_ID_PIC = 2;

    int count;

    ArrayList<SingleTeacher> singleTeacherArrayList;
    RecyclerView teachersRecView;
    SingleTeacherAdapter adapter;
    LinearLayoutManager layoutManager;


    private RewardedVideoAd mAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(Constants.storage,MODE_PRIVATE);

        MobileAds.initialize(this, "ca-app-pub-1192372351625384~9481952555");

        singleTeacherArrayList = new ArrayList<>();
        teachersRecView = (RecyclerView) findViewById(R.id.teachersRecView);
        teachersRecView.setNestedScrollingEnabled(false);
        teachersRecView.setHasFixedSize(true);
        adapter = new SingleTeacherAdapter(this,singleTeacherArrayList);
        layoutManager = new LinearLayoutManager(this);
        teachersRecView.setLayoutManager(layoutManager);
        teachersRecView.setAdapter(adapter);

        // Use an activity context to get the rewarded video instance.
        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            // Required to reward the user.
            @Override
            public void onRewarded(RewardItem reward) {
                Toast.makeText(HomeScreenActivity.this, "onRewarded! currency: " + reward.getType() + "  amount: " +
                        reward.getAmount(), Toast.LENGTH_SHORT).show();
                loadRewardedVideoAd();
                // Reward the user.
            }

            // The following listener methods are optional.
            @Override
            public void onRewardedVideoAdLeftApplication() {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoAdLeftApplication",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdClosed() {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdLoaded() {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoStarted() {
                Toast.makeText(HomeScreenActivity.this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
            }


        });

        loadRewardedVideoAd();

        String proPicUrl = null, userName = null, userEmail = null;
        int gender = 0;

        try {
            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(Constants.loginDetails,null));
            proPicUrl = jsonObject.getJSONObject("user").getString("profile_image");
            userName = jsonObject.getJSONObject("user").getString("name");
            userEmail = jsonObject.getJSONObject("user").getString("email");
            gender = jsonObject.getJSONObject("user").getInt("gender");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPreferences.contains("total_item")){
                    int  count = sharedPreferences.getInt("total_item",1);
                    int increasedCount = count+1;
                    getAllTeachers(increasedCount);
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        proPic = (ImageView) header.findViewById(R.id.proPic);
        nameOfUser = (TextView) header.findViewById(R.id.nameOfUser);
        emailOfUser = (TextView) header.findViewById(R.id.emailOfUser);

        nameOfUser.setText(userName);
        emailOfUser.setText(userEmail);

        Log.i("Result_url",proPicUrl);
        
        if (gender == 0){
            Glide.with(this)
                    .load(proPicUrl)
                    .placeholder(R.drawable.male_profile_pic)
                    .error(R.drawable.male_profile_pic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(proPic);
        } else {
            Glide.with(this)
                    .load(proPicUrl)
                    .placeholder(R.drawable.female_profile_pic)
                    .error(R.drawable.female_profile_pic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(proPic);

        }

        count = 1;

        getAllTeachers(count);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            if (exit) {
                finish(); // finish activity
            } else {
                Toast.makeText(this, "Press Back again to Exit.",
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logout){
            logout();
        } else if (id == R.id.changePw){
            changePassword();
        } else if (id == R.id.updateProPic){
            setIdOrProfilePic(PICK_PROFILE_PIC);
        } else if (id == R.id.updateIdCard){
            setIdOrProfilePic(PICK_ID_PIC);
        } else if (id == R.id.editProfile){
            editProfileStep1(sharedPreferences.getString(Constants.loginUserType,null));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout(){

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this);
        builder.setTitle("Logout?");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor = sharedPreferences.edit();
                editor.remove(Constants.loginDetails);
                editor.remove(Constants.loginUserType);
                editor.remove(Constants.loginPw);
                editor.remove(Constants.loginEmail);
                editor.apply();

                Intent i = new Intent(HomeScreenActivity.this,LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void changePassword(){
        View view = LayoutInflater.from(this).inflate(R.layout.z_change_password,null);

        final EditText oldPw = (EditText) view.findViewById(R.id.oldPassword);
        final EditText newPw = (EditText) view.findViewById(R.id.newPassword);
        final EditText confirmPw = (EditText) view.findViewById(R.id.confirmPassword);
        Button changeNow = (Button) view.findViewById(R.id.changeNow);

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this);
        builder.setView(view);

        AlertDialog alert = builder.create();
        alert.show();

        changeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!String.valueOf(oldPw.getText()).isEmpty() &&
                    !String.valueOf(newPw.getText()).isEmpty() &&
                    !String.valueOf(confirmPw.getText()).isEmpty()){

                    if (String.valueOf(newPw.getText()).equals(String.valueOf(confirmPw.getText()))){
                        try {
                            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(Constants.loginDetails,null));

                            int id = jsonObject.getJSONObject("user").getInt("id");
                            String tableName = jsonObject.getString("table_name");
                            String oldPassword = String.valueOf(oldPw.getText());
                            String newPassword = String.valueOf(newPw.getText());

                            changePasswordNow(id,tableName,oldPassword,newPassword);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(HomeScreenActivity.this,"Confirm Password doesn't match",Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(HomeScreenActivity.this,"All fields are necessary",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void changePasswordNow(final int id, final String tableName, final String oldPassword, final String newPassword){
        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Changing Password...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/changepassword",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),
                                    Toast.LENGTH_SHORT).show();

                            if (!jsonObject.getBoolean("error")){
                                editor = sharedPreferences.edit();
                                editor.remove(Constants.loginDetails);
                                editor.remove(Constants.loginUserType);
                                editor.remove(Constants.loginPw);
                                editor.remove(Constants.loginEmail);
                                editor.apply();

                                Intent i = new Intent(HomeScreenActivity.this,LoginActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("Auxi",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(id));
                params.put("table_name",tableName);
                params.put("old_password", oldPassword);
                params.put("new_password", newPassword);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    public void setIdOrProfilePic(int requestCode){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select image"),requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode==PICK_PROFILE_PIC || requestCode==PICK_ID_PIC) &&
                resultCode==RESULT_OK && data!=null && data.getData()!= null){
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
                byte[] bytes = baos.toByteArray();

                String encodedString = Base64.encodeToString(bytes,Base64.DEFAULT);

                changeImage(requestCode,encodedString);

                Log.i("Result",encodedString);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(HomeScreenActivity.this,"No Image selected",Toast.LENGTH_SHORT).show();
        }
    }

    public void changeImage(int requestCode, final String encodedString){
        JSONObject jsonObject = null;
        int id = 0;
        String tableName = null, dpOrId = null;
        try {
            jsonObject = new JSONObject(sharedPreferences.getString(Constants.loginDetails,null));

            id = jsonObject.getJSONObject("user").getInt("id");
            tableName = jsonObject.getString("table_name");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (requestCode == PICK_PROFILE_PIC){
            dpOrId = "D";
        } else if (requestCode == PICK_ID_PIC){
            dpOrId = "I";
        }

        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Updating Pic...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        final int finalId = id;
        final String finalTableName = tableName;
        final String finalDpOrId = dpOrId;
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/updatepic",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),
                                    Toast.LENGTH_SHORT).show();

                            login(sharedPreferences.getString(Constants.loginEmail,null),
                                    sharedPreferences.getString(Constants.loginPw,null),
                                    sharedPreferences.getString(Constants.loginUserType,null));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("Auxi",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(finalId));
                params.put("table_name", finalTableName);
                params.put("DPorID", finalDpOrId);
                params.put("base64", "data:image/jpeg;base64,"+encodedString);
                params.put("image_type", "jpeg");
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    public void login(final String email, final String pw, final String userType){
        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Getting updates...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (!jsonObject.getBoolean("error")){

                                editor = sharedPreferences.edit();
                                editor.putString(Constants.loginDetails,response);
                                editor.putString(Constants.loginEmail,email);
                                editor.putString(Constants.loginPw,pw);
                                editor.putString(Constants.loginUserType,userType);
                                editor.apply();

                                Intent i = new Intent(HomeScreenActivity.this,HomeScreenActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(i);
                            } else {
                                Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("Auxi",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email",email);
                params.put("password",pw);
                params.put("table_name", userType);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    public void editProfileStep1(String userType){
        View view = null;

        if (userType.equals("normal_user")){
            editProfileStep2_1();
        } else  if (userType.equals("teachers")){
            editProfileStep2_2();
        }
    }

    public void editProfileStep2_1(){
        View view = LayoutInflater.from(this).inflate(R.layout.z_update_details_normal_user,null);

        final EditText name = (EditText) view.findViewById(R.id.name);
        final EditText mobNumber = (EditText) view.findViewById(R.id.mobNumber);
        final Spinner location = (Spinner) view.findViewById(R.id.location);
        Button update = (Button) view.findViewById(R.id.update);

        int id = 0;

        try {
            JSONObject savedData = new JSONObject(sharedPreferences.getString(Constants.loginDetails,null));

            name.setText(savedData.getJSONObject("user").getString("name"));
            mobNumber.setText(savedData.getJSONObject("user").getString("mob"));

            switch (savedData.getJSONObject("user").getString("location")){
                case "Pantnagar":
                    location.setSelection(1);
                    break;

                case "Haldwani":
                    location.setSelection(2);
                    break;

                case "Rudrapur":
                    location.setSelection(3);
                    break;
            }

            id = savedData.getJSONObject("user").getInt("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final int finalId = id;
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!String.valueOf(name.getText()).isEmpty() &&
                        !String.valueOf(mobNumber.getText()).isEmpty() &&
                        location.getSelectedItemPosition()!=0){

                    if (mobNumber.getText().length() != 10){
                        Toast.makeText(HomeScreenActivity.this,"Please enter 10-digit valid mob number",Toast.LENGTH_SHORT).show();
                    } else {

                        final ProgressDialog progressDialog = new ProgressDialog(HomeScreenActivity.this,ProgressDialog.STYLE_SPINNER);
                        progressDialog.setTitle("Updating Profile...");
                        progressDialog.setMessage("Please wait...");
                        progressDialog.show();

                        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                                Constants.base_url + "v1/updatedetails/normal_user",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.hide();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);

                                            Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),
                                                    Toast.LENGTH_SHORT).show();

                                            login(sharedPreferences.getString(Constants.loginEmail,null),
                                                    sharedPreferences.getString(Constants.loginPw,null),
                                                    sharedPreferences.getString(Constants.loginUserType,null));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("Auxi",response);
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.hide();
                                error.printStackTrace();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("id", String.valueOf(finalId));
                                params.put("name", String.valueOf(name.getText()));
                                params.put("mob", String.valueOf(mobNumber.getText()));
                                params.put("location", String.valueOf(location.getSelectedItem()));
                                return params;
                            }
                        };

                        RequestQueue queue = Volley.newRequestQueue(HomeScreenActivity.this);
                        queue.add(stringRequest);

                    }

                } else {
                    Toast.makeText(HomeScreenActivity.this,"All fields are necessary",Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this);
        builder.setView(view);

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void editProfileStep2_2(){
        View view = LayoutInflater.from(this).inflate(R.layout.z_update_details_teachers,null);

        final EditText name = (EditText) view.findViewById(R.id.name);
        final EditText mobNumber = (EditText) view.findViewById(R.id.mobNumber);
        final EditText eduQual = (EditText) view.findViewById(R.id.eduQual);
        final EditText speciality = (EditText) view.findViewById(R.id.speciality);
        final EditText experience = (EditText) view.findViewById(R.id.experience);
        final EditText classUpto = (EditText) view.findViewById(R.id.classUpto);
        final EditText teachingSub = (EditText) view.findViewById(R.id.teachingSub);
        final EditText address = (EditText) view.findViewById(R.id.address);
        final Spinner location = (Spinner) view.findViewById(R.id.location);
        final Spinner availAs = (Spinner) view.findViewById(R.id.availAs);
        Button update = (Button) view.findViewById(R.id.update);

        int id = 0;

        try {
            JSONObject savedData = new JSONObject(sharedPreferences.getString(Constants.loginDetails,null));

            name.setText(savedData.getJSONObject("user").getString("name"));
            mobNumber.setText(savedData.getJSONObject("user").getString("mob"));
            eduQual.setText(savedData.getJSONObject("user").getString("edu_qualification"));
            speciality.setText(savedData.getJSONObject("user").getString("speciality_in"));
            experience.setText(savedData.getJSONObject("user").getString("experience"));
            classUpto.setText(savedData.getJSONObject("user").getString("class_upto"));
            teachingSub.setText(savedData.getJSONObject("user").getString("teaching_sub"));
            address.setText(savedData.getJSONObject("user").getString("address"));

            switch (savedData.getJSONObject("user").getString("location")){
                case "Pantnagar":
                    location.setSelection(1);
                    break;

                case "Haldwani":
                    location.setSelection(2);
                    break;

                case "Rudrapur":
                    location.setSelection(3);
                    break;
            }

            switch (savedData.getJSONObject("user").getString("available_as")){
                case "Home Tutor":
                    availAs.setSelection(1);
                    break;

                case "Coaching Centre":
                    availAs.setSelection(2);
                    break;
            }

            id = savedData.getJSONObject("user").getInt("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final int finalId = id;
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!String.valueOf(name.getText()).isEmpty() &&
                        !String.valueOf(mobNumber.getText()).isEmpty() &&
                        !String.valueOf(eduQual.getText()).isEmpty() &&
                        !String.valueOf(speciality.getText()).isEmpty() &&
                        !String.valueOf(experience.getText()).isEmpty() &&
                        !String.valueOf(classUpto.getText()).isEmpty() &&
                        !String.valueOf(teachingSub.getText()).isEmpty() &&
                        !String.valueOf(address.getText()).isEmpty() &&
                        location.getSelectedItemPosition()!=0 &&
                        availAs.getSelectedItemPosition()!=0){

                    if (mobNumber.getText().length() != 10){
                        Toast.makeText(HomeScreenActivity.this,"Please enter 10-digit valid mob number",Toast.LENGTH_SHORT).show();
                    } else {

                        final ProgressDialog progressDialog = new ProgressDialog(HomeScreenActivity.this,ProgressDialog.STYLE_SPINNER);
                        progressDialog.setTitle("Updating Profile...");
                        progressDialog.setMessage("Please wait...");
                        progressDialog.show();

                        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                                Constants.base_url + "v1/updatedetails/teachers",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        progressDialog.hide();

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);

                                            Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),
                                                    Toast.LENGTH_SHORT).show();

                                            login(sharedPreferences.getString(Constants.loginEmail,null),
                                                    sharedPreferences.getString(Constants.loginPw,null),
                                                    sharedPreferences.getString(Constants.loginUserType,null));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("Auxi",response);
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.hide();
                                error.printStackTrace();
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("id", String.valueOf(finalId));
                                params.put("name", String.valueOf(name.getText()));
                                params.put("mob", String.valueOf(mobNumber.getText()));
                                params.put("location", String.valueOf(location.getSelectedItem()));
                                params.put("available_as", String.valueOf(availAs.getSelectedItem()));
                                params.put("edu_qualification", String.valueOf(eduQual.getText()));
                                params.put("speciality_in", String.valueOf(speciality.getText()));
                                params.put("experience", String.valueOf(experience.getText()));
                                params.put("class_upto", String.valueOf(classUpto.getText()));
                                params.put("teaching_sub", String.valueOf(teachingSub.getText()));
                                params.put("address", String.valueOf(address.getText()));
                                return params;
                            }
                        };

                        RequestQueue queue = Volley.newRequestQueue(HomeScreenActivity.this);
                        queue.add(stringRequest);

                    }

                } else {
                    Toast.makeText(HomeScreenActivity.this,"All fields are necessary",Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this);
        builder.setView(view);

        AlertDialog alert = builder.create();
        alert.show();

    }

    public void getAllTeachers(final int count){
        final ProgressDialog progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Getting all teachers...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        editor = sharedPreferences.edit();
        editor.putInt("total_item",count);
        editor.apply();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.base_url + "v1/getallusers",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.hide();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (!jsonObject.getBoolean("error")){

                        int totalUsers = jsonObject.getInt("total_users");

                        JSONArray usersArray = jsonObject.getJSONArray("users");

                        if (usersArray.length()>0){
                            for (int i = 0; i<usersArray.length(); i++){

                                JSONObject singleUser = usersArray.getJSONObject(i);

                                int id = singleUser.getInt("id");
                                String name = singleUser.getString("name");
                                String email = singleUser.getString("email");
                                int gender = singleUser.getInt("gender");
                                String mob = singleUser.getString("mob");
                                String location = singleUser.getString("location");
                                String profilePic = singleUser.getString("profile_image");
                                String idCard = singleUser.getString("id_card");
                                String apiKey = singleUser.getString("api_key");
                                int status = singleUser.getInt("status");
                                String userType = singleUser.getString("user_type");
                                String availAs = singleUser.getString("available_as");
                                String eduQual = singleUser.getString("edu_qualification");
                                String speciality = singleUser.getString("speciality_in");
                                int experience = singleUser.getInt("experience");
                                String classUpto = singleUser.getString("class_upto");
                                String teachingSub = singleUser.getString("teaching_sub");
                                String address = singleUser.getString("address");
                                double rating = singleUser.getDouble("rating");
                                int votingCount = singleUser.getInt("voting_count");

                                SingleTeacher singleTeacher = new SingleTeacher(id,gender,status,experience,votingCount,
                                        name,email,mob,location,profilePic,idCard,apiKey,userType,availAs,eduQual,speciality,
                                        classUpto,teachingSub,address,rating);

                                singleTeacherArrayList.add(singleTeacher);
                                adapter.notifyDataSetChanged();
                            }

                            adapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(HomeScreenActivity.this,"No more data available",Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(HomeScreenActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.hide();
                Toast.makeText(HomeScreenActivity.this,"Some error has occurred. Please restart the app with active internet connection",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("page_number",String.valueOf(count));
                params.put("table_name","teachers");
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);

    }

    private void loadRewardedVideoAd() {
        mAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder()
                        .addTestDevice("006984C9E646A32DE1146DCF8C9662AF")
                        .build());
    }

    public void showRewardedAdIfLoaded(){
        if (mAd.isLoaded()) {
            mAd.show();
        }
    }

    @Override
    public void onResume() {
        mAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        mAd.pause(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mAd.destroy(this);
        super.onDestroy();
    }

}
